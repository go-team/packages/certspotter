#!/usr/bin/make -f

include /usr/share/dpkg/pkg-info.mk
export GOFLAGS = -ldflags=-X=main.Version=$(DEB_VERSION_UPSTREAM)

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

%:
	dh $@ --buildsystem=golang

# the source's root is a library that includes subdirectories that are
# sub-packages, such as "ct" (Google's CT code) and "cmd/certspotter" and
# "cmd/ctparsewatch" for the actual resulting binaries. We actually want to
# package mostly "cmd/certspotter" in this package and it's hard but pointless
# to package the library into a separate source/binary package.  Force the
# golang dh buildsystem to actually produce a "program", and forcibly exclude
# the binaries that which we don't want to ship (such as ctparsewatch).
override_dh_auto_install:
	dh_auto_install -- --no-source
	rm -f debian/certspotter/usr/bin/ctparsewatch debian/certspotter/usr/bin/submitct

ifeq (,$(findstring nodoc,$(DEB_BUILD_OPTIONS) $(DEB_BUILD_PROFILES)))
MANPAGES = debian/certspotter.8 debian/certspotter-script.8

debian/%.8: man/%.md debian/changelog
	lowdown -s -Tman \
		-M title:$(basename $(notdir $@)) \
		-M section:$(subst .,,$(suffix $@)) \
		-M date:$(if $(SOURCE_DATE_EPOCH),$(shell date -I -u -d "@$(SOURCE_DATE_EPOCH)"),$(shell date -I -u)) \
		-o $@ $<

execute_after_dh_auto_build: $(MANPAGES)

execute_after_dh_installman:
	dh_installman $(MANPAGES)

execute_after_dh_auto_clean:
	dh_clean $(MANPAGES)
endif
